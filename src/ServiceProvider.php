<?php

namespace Login\SocialOs;

use Login\SocialOs\Models\SocialOsUser;
use Login\SocialOs\Provider\SocialOsUserProvider;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function __construct()
    {
        // constructor body
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/social.php' => config_path('social.php'),
        ]);

        auth()->provider("SocialOsUserProvider", function ($app, array $config) {
            return new SocialOsUserProvider();
        });
    }

    public function register()
    {

    }
}
